import { User } from 'models';
import { Logger } from 'services';
import { Request, Response } from 'express';
import { createAccessToken, createRefreshToken } from 'services';

import bcrypt from 'bcryptjs';
import Objection from 'objection';

export const Register = async (req: Request, res: Response) => {
  try {
    const { email, username, password } = req.body;

    const salt = await bcrypt.genSalt();
    const hashedPass = await bcrypt.hash(password, salt);

    const user = await User.query().insert({
      email,
      username,
      password: hashedPass,
    });

    res.status(201).send({
      user,
      accessToken: createAccessToken(user),
      refreshToken: createRefreshToken(user),
    });
  } catch (e) {
    Logger.error(e);
    if (e instanceof Objection.UniqueViolationError) {
      res.status(400).send({
        message: {
          en: 'User already exist!',
        },
      });
    } else
      res.status(400).send({
        message: e.constraint,
      });
  }
};

export const Login = async (req: Request, res: Response) => {
  try {
    const { username, password } = req.body;

    const user = await User.query().findOne('username', username);
    if (!user) {
      return res.status(401).send({
        message: 'Bad Username or Password',
      });
    } else {
      const validPass = await bcrypt.compare(password, user.password);
      if (!validPass) {
        return res.status(401).send({
          message: 'Bad Username or Password',
        });
      }

      res.send({
        user,
        accessToken: createAccessToken(user),
        refreshToken: createRefreshToken(user),
      });
    }
  } catch (e) {
    Logger.error(e.message);
    res.status(400).send({
      message: e.data,
    });
  }
};
