import { Checklist, Task, User } from 'models';
import { Request, Response } from 'express';

export const add = async (req: Request, res: Response) => {
  // @ts-ignore
  const { id } = req.payload;
  const { title } = req.body;
  const checklist = await Checklist.query().insert({
    title,
    user_id: id,
  });

  const user = await User.query().findById(id);
  await user.$relatedQuery('checklists').relate(checklist);
  return res.send({
    data: checklist,
  });
};

export const getChecklist = async (req: Request, res: Response) => {
  const { id } = req.params;
  const checklist = await Checklist.query()
    .findById(id)
    .withGraphFetched('tasks');

  return res.send({
    data: checklist,
  });
};

export const getChecklists = async (req: Request, res: Response) => {
  // @ts-ignore
  const { id } = req.payload;
  const checklists = await User.query()
    .findById(id)
    .withGraphFetched('checklists');

  return res.send({
    data: checklists,
  });
};

export const updateChecklist = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { title } = req.body;
  const checklist = await Checklist.query().findById(id).patch({
    title,
  });

  return res.send({
    data: checklist,
  });
};

export const deleteChecklist = async (req: Request, res: Response) => {
  const { id } = req.params;
  const checklist = await Checklist.query().deleteById(id);

  console.log({ checklist });
  return res.send({
    data: true,
  });
};
