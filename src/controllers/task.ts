import { User, Task, Checklist } from 'models';
import { Request, Response } from 'express';

export const addTask = async (req: Request, res: Response) => {
  // @ts-ignore
  const { id } = req.payload;
  const { checklistId, title } = req.body;
  const task = await Task.query().insert({
    title,
    user_id: id,
    list_id: checklistId,
  });

  const user = await User.query().findById(id);
  const checklist = await Checklist.query().findById(checklistId);

  await task.$relatedQuery('user').relate(user);
  await task.$relatedQuery('checklist').relate(checklist);
  return res.send({
    data: task,
  });
};

export const getTasks = async (req: Request, res: Response) => {
  // @ts-ignore
  const { id } = req.payload;
  const { status } = req.body;
  const tasks = await User.query()
    .findById(id)
    .withGraphFetched('tasks')
    .modifyGraph('tasks', (builder) => {
      builder.where('status', status);
    });

  return res.send({
    data: tasks,
  });
};

export const updateTask = async (req: Request, res: Response) => {
  const { id } = req.params;
  const { title, status } = req.body;
  const task = await Task.query()
    .findById(id)
    .patch({
      title,
      status,
    })
    .returning('*');

  return res.send({
    data: task,
  });
};

export const deleteTask = async (req: Request, res: Response) => {
  const { id } = req.params;
  const task = await Task.query().deleteById(id);

  console.log({ task });
  return res.send({
    data: true,
  });
};
