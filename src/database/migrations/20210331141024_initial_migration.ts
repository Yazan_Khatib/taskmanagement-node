import { Knex } from 'knex';

export async function up(knex: Knex): Promise<void> {
  return knex.schema
    .createTable('users', (table) => {
      table.increments('id').primary();
      table.string('email');
      table.string('username');
      table.string('password');
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
    })

    .createTable('checklists', (table) => {
      table.increments('id').primary();
      table.string('title');
      table.integer('user_id').notNullable();
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
    })

    .createTable('tasks', (table) => {
      table.increments('id').primary();
      table.string('title');
      table.integer('user_id').notNullable();
      table.integer('list_id').notNullable();
      table.boolean('status').defaultTo(false);
      table.timestamp('created_at').defaultTo(knex.fn.now());
      table.timestamp('updated_at').defaultTo(knex.fn.now());
    });
}

export async function down(knex: Knex): Promise<void> {
  return knex.schema
    .dropTableIfExists('users')
    .dropTableIfExists('checklists')
    .dropTableIfExists('tasks');
}
