import { Model } from 'objection';
import { User, Task } from 'models';

export class Checklist extends Model {
  readonly id!: number;
  title: string;
  user_id: number;
  createdAt?: Date;
  updatedAt?: Date;

  static get tableName() {
    return 'checklists';
  }

  static get idColumn() {
    return 'id';
  }

  static relationMappings = () => ({
    user: {
      relation: Model.BelongsToOneRelation,
      modelClass: User,
      join: {
        from: 'checklists.user_id',
        to: 'users.id',
      },
    },

    tasks: {
      relation: Model.HasManyRelation,
      modelClass: Task,
      join: {
        from: 'checklists.id',
        to: 'tasks.list_id',
      },
    },
  });
}
