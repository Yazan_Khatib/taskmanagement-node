import { Model } from 'objection';
import { User, Checklist } from 'models';

export class Task extends Model {
  readonly id!: number;
  title: string;
  status: boolean;
  user_id: number;
  list_id: number;
  createdAt?: Date;
  updatedAt?: Date;

  static get tableName() {
    return 'tasks';
  }

  static get idColumn() {
    return 'id';
  }

  static relationMappings = () => ({
    user: {
      relation: Model.BelongsToOneRelation,
      modelClass: User,
      join: {
        from: 'tasks.user_id',
        to: 'users.id',
      },
    },

    checklist: {
      relation: Model.BelongsToOneRelation,
      modelClass: Checklist,
      join: {
        from: 'tasks.list_id',
        to: 'checklists.id',
      },
    },
  });
}
