import { Model } from 'objection';
import { Task, Checklist } from 'models';

export class User extends Model {
  readonly id!: number;
  email!: string;
  username!: string;
  password: string;
  createdAt?: Date;
  updatedAt?: Date;

  static get tableName() {
    return 'users';
  }

  static get idColumn() {
    return 'id';
  }

  static relationMappings = () => ({
    checklists: {
      relation: Model.HasManyRelation,
      modelClass: Checklist,
      join: {
        from: 'users.id',
        to: 'checklists.user_id',
      },
    },

    tasks: {
      relation: Model.HasManyRelation,
      modelClass: Task,
      join: {
        from: 'users.id',
        to: 'tasks.user_id',
      },
    },
  });
}
