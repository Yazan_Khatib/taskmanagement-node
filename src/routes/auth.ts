import { Router } from 'express';
import { Login, Register } from 'controllers';

const router = Router();

// User Auth Routes
router.post('/register', Register);
router.post('/login', Login);

export { router as authRoutes };
