import { Router } from 'express';
import {
  add,
  getChecklist,
  getChecklists,
  updateChecklist,
  deleteChecklist,
} from 'controllers';
import { isAuth } from 'services';

const router = Router();

// User Checklist Routes
router.post('/', isAuth, add);
router.get('/', isAuth, getChecklists);
router.get('/:id', isAuth, getChecklist);
router.patch('/:id', isAuth, updateChecklist);
router.delete('/:id', isAuth, deleteChecklist);

export { router as checklistRoutes };
