import { Router } from 'express';
import { isAuth } from 'services';
import { addTask, getTasks, updateTask, deleteTask } from 'controllers';

const router = Router();

// User Task Routes
router.post('/', isAuth, addTask);
router.get('/', isAuth, getTasks);
router.patch('/:id', isAuth, updateTask);
router.delete('/:id', isAuth, deleteTask);

export { router as taskRoutes };
