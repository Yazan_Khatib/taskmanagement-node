import { Request, Response, NextFunction } from 'express';
import { User } from 'models';
import { Logger } from 'services';
import { sign, verify } from 'jsonwebtoken';

export const createAccessToken = (user: User) => {
  const accessToken = sign({ id: user.id }, process.env.ACCESS_TOKEN_SECRET!, {
    expiresIn: '2h',
  });

  return accessToken;
};

export const createRefreshToken = (user: User) => {
  const refreshToken = sign(
    { id: user.id },
    process.env.REFRESH_TOKEN_SECRET!,
    {
      expiresIn: '7d',
    },
  );

  return refreshToken;
};

export const isAuth = (req: Request, res: Response, next: NextFunction) => {
  const authorization = req.headers['authorization'];
  if (!authorization) {
    return res.status(401).send({
      message: 'not authenticated!',
    });
  }

  try {
    const token = authorization.split(' ')[1];
    const payload = verify(token, process.env.ACCESS_TOKEN_SECRET!);
    //@ts-ignore
    req.payload = payload;
  } catch (err) {
    Logger.error(err);
    return res.status(401).send({
      message: 'not authenticated!',
    });
  }

  return next();
};
